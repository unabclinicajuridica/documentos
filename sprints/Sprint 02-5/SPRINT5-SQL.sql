ALTER TABLE `asignacion_agenda` ADD UNIQUE( `fecha_asignacion`, `hora_inicio`, `rut`); 

ALTER TABLE `audiencias` 
CHANGE `descripcion` `descripcion` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, 
CHANGE `tipo_audiencia` `tipo_audiencia` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;
CHANGE `evaluacion_completada` `evaluacion_completada` TINYINT(1) NOT NULL DEFAULT '0';

ALTER TABLE `audiencias` ADD `estado_revision` VARCHAR(17) NOT NULL DEFAULT 'EN_ESPERA_LLENADO' 
COMMENT 'EN_ESPERA_LLENADO, PENDIENTE, EVALUADA, LEIDO, CERRADA' AFTER `evaluacion_completada`;

---------------------

ALTER TABLE `audiencias` ADD `comentario_abogado` TEXT NULL DEFAULT NULL AFTER `estado_revision`;

ALTER TABLE `privilegios` ADD INDEX(`sede`);
ALTER TABLE `usuario_sede` CHANGE `id_sede` `id_sede` INT NOT NULL; 
ALTER TABLE `conf_agenda` CHANGE `sede` `sede` INT NOT NULL; 
ALTER TABLE `conf_agenda` ADD INDEX(`sede`);
ALTER TABLE `audiencias` CHANGE `id_causa` `id_causa` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ROL de la causa';
ALTER TABLE `audiencias` ADD INDEX(`id_causa`);
ALTER TABLE `audiencias` CHANGE `rut_alumno` `rut_alumno` INT NULL DEFAULT NULL;
ALTER TABLE `audiencias` ADD INDEX(`rut_alumno`);
ALTER TABLE `audiencias` CHANGE `rut_profesor` `rut_profesor` INT NULL DEFAULT NULL; 
ALTER TABLE `audiencias` ADD INDEX(`rut_profesor`);
ALTER TABLE `audiencias` CHANGE `sede` `sede` INT NOT NULL; 
ALTER TABLE `audiencias` ADD INDEX(`sede`); 
ALTER TABLE `audiencias` ADD CONSTRAINT `fk_audiencia_alumno` FOREIGN KEY (`rut_alumno`) REFERENCES `clinicajuridica`.`usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `audiencias` ADD CONSTRAINT `fk_audiencia_abogado` FOREIGN KEY (`rut_profesor`) REFERENCES `clinicajuridica`.`usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `audiencias` ADD CONSTRAINT `fk_audiencia_sede` FOREIGN KEY (`sede`) REFERENCES `clinicajuridica`.`sedes`(`id_sede`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `audiencias` ADD CONSTRAINT `fk_audiencia_causa` FOREIGN KEY (`id_causa`) REFERENCES `clinicajuridica`.`causas`(`id_causa`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `privilegios` ADD CONSTRAINT `fk_privilegio_usuario` FOREIGN KEY (`rut`) REFERENCES `clinicajuridica`.`usuarios`(`rut`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `conf_agenda` CHANGE `rut` `rut` INT NOT NULL; 
ALTER TABLE `conf_agenda` ADD CONSTRAINT `fk_confag_usuario` FOREIGN KEY (`rut`) REFERENCES `clinicajuridica`.`usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `usuario_sede` ADD CONSTRAINT `fk_ussede_usuario` FOREIGN KEY (`rut`) REFERENCES `clinicajuridica`.`usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `asignacion_agenda` CHANGE `id_asunto` `id_asunto` INT NULL DEFAULT NULL;
ALTER TABLE `asignacion_agenda` ADD CONSTRAINT `fk_asigagen_asunto` FOREIGN KEY (`id_asunto`) REFERENCES `clinicajuridica`.`asuntos`(`id_asunto`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `asignacion_agenda` CHANGE `sede` `sede` INT NOT NULL; 
ALTER TABLE `asignacion_agenda` ADD CONSTRAINT `fk_asisagen_sede` FOREIGN KEY (`sede`) REFERENCES `clinicajuridica`.`sedes`(`id_sede`) ON DELETE RESTRICT ON UPDATE RESTRICT; 