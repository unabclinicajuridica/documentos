 
ALTER TABLE `clientes` ADD `edad` TINYINT NOT NULL DEFAULT '0' AFTER `email`; 
--------------------------------------
CREATE TABLE `competencia` (
  `id` int(11) NOT NULL,
  `nombre_largo` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre_corto` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL
);

ALTER TABLE `competencia`
ADD PRIMARY KEY (`id`);

ALTER TABLE `competencia`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

CREATE TABLE `procedimiento` ( 
	`id` INT NOT NULL AUTO_INCREMENT , 
	`nombre_largo` VARCHAR(64) NOT NULL , 
	`nombre_corto` VARCHAR(32) NOT NULL , PRIMARY KEY (`id`)
) ENGINE = InnoDB;

INSERT INTO `competencia` (`id`, `nombre_largo`, `nombre_corto`) VALUES 
('1', 'COMPETENCIA 1', 'Competencia 1'), 
('2', 'COMPETENCIA 2', 'Competencia 2'), 
('3', 'COMPETENCIA 3', 'Competencia 3');

INSERT INTO `procedimiento` (`id`, `nombre_largo`, `nombre_corto`) VALUES 
('1', 'PROCEDIMIENTO 1', 'Procedimiento 1'), 
('2', 'PROCEDIMIENTO 2', 'Procedimiento 2'), 
('3', 'PROCEDIMIENTO 3', 'Procedimiento 3');