CREATE TABLE `tribunal` ( 
`id` INT NOT NULL AUTO_INCREMENT , 
`nombre` VARCHAR(64) NOT NULL , 
`rut` VARCHAR(11) NOT NULL , 
`telefono` VARCHAR(24) NOT NULL , 
`correo` VARCHAR(64) NULL , 
`cuenta_corriente` CHAR(16) NULL , 
`direccion` VARCHAR(128) NOT NULL , 
`nombre_largo` VARCHAR(128) NOT NULL , 
`rut_sin_dv` INT(9) NOT NULL , 
`dv` INT(1) NOT NULL , 
`telefonos` VARCHAR(128) NULL , 
`correos` VARCHAR(128) NULL , 
`region` VARCHAR(24) NOT NULL , 
`tipo_tribunal` VARCHAR(64) NOT NULL ,
PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `causas` ADD `id` INT(11) NOT NULL AUTO_INCREMENT FIRST, ADD UNIQUE (`id`);
ALTER TABLE `causas` DROP PRIMARY KEY, ADD PRIMARY KEY(`id`);

ALTER TABLE `causas` CHANGE `id_causa` `id_causa` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

ALTER TABLE `causas` ADD `ID_TRIBUNAL` INT NULL AFTER `SEDE`, ADD INDEX (`ID_TRIBUNAL`);

ALTER TABLE `causas` ADD CONSTRAINT `FK_Causas_Tribunal` FOREIGN KEY (`ID_TRIBUNAL`) REFERENCES `tribunal`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

----------------------

INSERT INTO `tribunal` (`id`, `nombre`, `rut`, `telefono`, `correo`, `cuenta_corriente`, `direccion`, `nombre_largo`, `rut_sin_dv`, `dv`, `telefonos`, `correos`, `region`, `tipo_tribunal`) VALUES
(1, 'JUICIO ORAL EN LO PENAL DE VIÑA DEL MAR', '61973400-9', '(32) 2795243', 'notifica_top_vinadelmar@pjud.cl', '238-0-000630-9', 'ALVAREZ N° 1330', 'TRIBUNAL JUICIO ORAL EN LO PENAL DE VIÑA DEL MAR', 61973400, 9, NULL, NULL, 'Valparaiso', 'PRIMERA INSTANCIA');

INSERT INTO `tribunal` (`id`, `nombre`, `rut`, `telefono`, `correo`, `cuenta_corriente`, `direccion`, `nombre_largo`, `rut_sin_dv`, `dv`, `telefonos`, `correos`, `region`, `tipo_tribunal`) VALUES (NULL, '1° JUZGADO CIVIL DE VIÑA DEL MAR', '0-', '(32) 682593', 'jc1_vinadelmar @pjud.cl', 'null', '3 NORTE N° 104 - VIÑA DEL MAR', '1° JUZGADO CIVIL DE VIÑA DEL MAR', '0', '0', NULL, NULL, 'Valparaiso', 'PRIMERA INSTANCIA');

INSERT INTO `tribunal` (`id`, `nombre`, `rut`, `telefono`, `correo`, `cuenta_corriente`, `direccion`, `nombre_largo`, `rut_sin_dv`, `dv`, `telefonos`, `correos`, `region`, `tipo_tribunal`) VALUES (NULL, 'CORTE DE APELACIONES DE VALPARAISO', '60305000-2', '(32) 2258357', 'ca_valparaiso @pjud.cl', '23.900.213.701', 'PLAZA DE JUSTICIA S/N. - VALPARAISO', 'CORTE DE APELACIONES DE VALPARAISO', '60305000', '2', NULL, NULL, 'Valparaiso', 'CORTE APELACIONES');

INSERT INTO `tribunal` (`id`, `nombre`, `rut`, `telefono`, `correo`, `cuenta_corriente`, `direccion`, `nombre_largo`, `rut_sin_dv`, `dv`, `telefonos`, `correos`, `region`, `tipo_tribunal`) VALUES (NULL, 'JUZGADO DE FAMILIA DE QUILPUE', '65552550-5', '(32) 2916965', 'jfquilpue@pjud.cl', '25.300.069.536', 'THOMPSON Nº 1282', 'JUZGADO DE FAMILIA DE QUILPUE', '65552550', '5', NULL, NULL, 'Valparaiso', 'PRIMERA INSTANCIA');



ALTER TABLE `asuntos` ADD `observaciones` VARCHAR(1024) NULL AFTER `sede`;

ALTER TABLE `asignacion_agenda` CHANGE `id_asunto` `id_asunto` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `asignacion_agenda` ADD `id_orientacion` INT(11) NULL DEFAULT NULL AFTER `id_asunto`;

ALTER TABLE `orientacion` ADD `titulo_descriptivo` VARCHAR(64) NULL DEFAULT NULL AFTER `fec_ingreso`, ADD `descripcion` VARCHAR(512) NULL DEFAULT NULL AFTER `titulo_descriptivo`;

ALTER TABLE `asignacion_agenda` DROP FOREIGN KEY `fk_asignacionagenda_causas`; 

-- ALTER TABLE `asignacion_agenda` ADD CONSTRAINT `fk_asignacionagenda_causas` FOREIGN KEY (`id_causa`) REFERENCES `clinicajuridica`.`causas`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `asignacion_agenda` ADD `titulo_evento` VARCHAR(128) NULL DEFAULT NULL AFTER `icaluid_evento`, ADD `descripcion_evento` VARCHAR(512) NULL DEFAULT NULL AFTER `titulo_evento`;

ALTER TABLE `orientacion` CHANGE `resena` `resena` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;


---------------------
ALTER TABLE `sedes` ADD `nombre_sede_corto` VARCHAR(24) NOT NULL DEFAULT 'Sede' AFTER `nombre_sede`;
UPDATE `sedes` SET `nombre_sede_corto` = 'Viña' WHERE `sedes`.`id_sede` = 1
UPDATE `sedes` SET `nombre_sede_corto` = 'Santiago' WHERE `sedes`.`id_sede` = 2
UPDATE `sedes` SET `nombre_sede_corto` = 'Concepción' WHERE `sedes`.`id_sede` = 3 
---------------------
ALTER TABLE `audiencias` CHANGE `numero_audiencia` `numero_audiencia` INT(11) NULL, CHANGE `rut_alumno` `rut_alumno` INT(255) NULL, CHANGE `rut_profesor` `rut_profesor` INT(255) NULL, CHANGE `nota_registro_1` `nota_registro_1` INT(2) NULL, CHANGE `nota_registro_2` `nota_registro_2` INT(2) NULL, CHANGE `nota_registro_3` `nota_registro_3` INT(2) NULL, CHANGE `nota_registro_otros` `nota_registro_otros` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL, CHANGE `nota_destreza_1` `nota_destreza_1` INT(2) NULL, CHANGE `nota_destreza_2` `nota_destreza_2` INT(2) NULL, CHANGE `nota_destreza_3` `nota_destreza_3` INT(2) NULL, CHANGE `nota_destreza_4` `nota_destreza_4` INT(2) NULL, CHANGE `nota_destreza_5` `nota_destreza_5` INT(2) NULL, CHANGE `nota_destreza_6` `nota_destreza_6` INT(2) NULL, CHANGE `nota_destreza_7` `nota_destreza_7` INT(2) NULL, CHANGE `nota_destreza_8` `nota_destreza_8` INT(2) NULL, CHANGE `nota_destreza_9` `nota_destreza_9` INT(2) NULL, CHANGE `nota_destreza_10` `nota_destreza_10` INT(2) NULL, CHANGE `nota_destreza_11` `nota_destreza_11` INT(2) NULL, CHANGE `nota_destreza_12` `nota_destreza_12` INT(2) NULL, CHANGE `nota_item_1` `nota_item_1` INT(2) NULL, CHANGE `nota_item_2` `nota_item_2` INT(2) NULL, CHANGE `nota_item_3` `nota_item_3` INT(2) NULL, CHANGE `nota_final` `nota_final` INT(2) NULL;