CREATE TABLE `rol` ( `id` INT NOT NULL , 
`nombre` VARCHAR(16) NOT NULL , 
`letra` CHAR(1) NOT NULL ,
`mantencion_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`busqueda_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`agenda_global` BOOLEAN NOT NULL DEFAULT FALSE,
`agenda_personal` BOOLEAN NOT NULL DEFAULT FALSE,
`agenda_agendar` BOOLEAN NOT NULL DEFAULT FALSE,
`asignar_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`evaluar` BOOLEAN NOT NULL DEFAULT FALSE,
`audiencia_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`audiencia_propia` BOOLEAN NOT NULL DEFAULT FALSE,
`audiencia_crear` BOOLEAN NOT NULL DEFAULT FALSE,
`orientacion_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`orientacion_propio` BOOLEAN NOT NULL DEFAULT FALSE,
`orientacion_crear` BOOLEAN NOT NULL DEFAULT FALSE,
`causa_todo` BOOLEAN NOT NULL DEFAULT FALSE,
`causa_propio` BOOLEAN NOT NULL DEFAULT FALSE,
`causa_crear` BOOLEAN NOT NULL DEFAULT FALSE,
`estadisticas_ver` BOOLEAN NOT NULL DEFAULT FALSE,
`reportes_ver` BOOLEAN NOT NULL DEFAULT FALSE,
`asunto_crear` BOOLEAN NOT NULL DEFAULT FALSE,
`tramite_crear` BOOLEAN NOT NULL DEFAULT FALSE,
PRIMARY KEY (`id`))
ENGINE = InnoDB;

ALTER TABLE `rol` ADD UNIQUE(`nombre`);
ALTER TABLE `rol` ADD UNIQUE(`letra`); 
ALTER TABLE `rol` ADD INDEX(`letra`); 

INSERT INTO `rol` (`id`, `nombre`, `letra`, `mantencion_todo`, `busqueda_todo`, `agenda_global`, `agenda_personal`, `agenda_agendar`, `asignar_todo`, `evaluar`, `audiencia_todo`, `audiencia_propia`, `audiencia_crear`, `orientacion_todo`, `orientacion_propio`, `orientacion_crear`, `causa_todo`, `causa_propio`, `causa_crear`, `estadisticas_ver`, `reportes_ver`, `asunto_crear`, `tramite_crear`) 
VALUES 
('1', 'alumno', 'A', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '0', '1', '0', '0', '1', '0', '0', '0', '0', '1'), 
('2', 'profesor', 'P', '0', '0', '0', '1', '0', '0', '1', '0', '1', '1', '0', '1', '1', '0', '1', '1', '0', '0', '1', '0'),
('3', 'secretario', 'F', '1', '1', '1', '0', '1', '1', '0', '1', '0', '1', '1', '0', '1', '1', '0', '1', '1', '1', '1', '0'), 
('4', 'director', 'D', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0'), 
('5', 'administrador', 'Z', '1', '1', '1', '0', '1', '1', '0', '1', '0', '1', '1', '0', '1', '1', '0', '1', '1', '1', '1', '0')

ALTER TABLE `usuarios` ADD `id_rol` INT NOT NULL AFTER `sede`; 
ALTER TABLE `usuarios` ADD INDEX(`id_rol`); 

ALTER TABLE `usuarios` ADD CONSTRAINT `fk_usuario_roles` FOREIGN KEY (`id_rol`) REFERENCES `rol`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `usuarios` ADD `id_ciforum_rol` INT NOT NULL DEFAULT '3' AFTER `id_rol`, ADD INDEX (`id_ciforum_rol`); 

ALTER TABLE `usuarios` ADD CONSTRAINT `fk_usuario_ciforum_rol` FOREIGN KEY (`id_ciforum_rol`) REFERENCES `clinicajuridica`.`ciforum_roles`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 

ALTER TABLE `usuarios` ADD `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (`id`); 

CREATE TABLE `ciforum_tema_visita` ( `id` INT NOT NULL AUTO_INCREMENT , `id_usuario` INT NOT NULL , `id_ciforum_tema` INT NOT NULL , `ultima_visita` DATETIME NOT NULL , PRIMARY KEY (`id`), UNIQUE (`id_usuario`, `id_ciforum_tema`)) ENGINE = InnoDB;

ALTER TABLE `ciforum_tema_visita` ADD CONSTRAINT `fk_visita_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `clinicajuridica`.`usuarios`(`id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `ciforum_tema_visita` ADD CONSTRAINT `fk_visita_tema` FOREIGN KEY (`id_ciforum_tema`) REFERENCES `clinicajuridica`.`ciforum_temas`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `orientacion` ADD `id_causa` INT NULL DEFAULT NULL COMMENT 'id correlativa (de tabla)' AFTER `sede`, ADD INDEX (`id_causa`); 

ALTER TABLE `orientacion` ADD CONSTRAINT `fk_orientacion_causa` FOREIGN KEY (`id_causa`) REFERENCES `clinicajuridica`.`causas`(`id`) ON DELETE SET NULL ON UPDATE CASCADE; 