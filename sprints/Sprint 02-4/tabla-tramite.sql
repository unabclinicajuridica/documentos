CREATE TABLE `tramite` ( `id_tramite` INT NOT NULL AUTO_INCREMENT , `fecha_ingreso` DATE NOT NULL , `hora_ingreso` TIME NOT NULL , `rut_usuario_alumno` INT NOT NULL , `descripcion` TEXT NOT NULL , `timestamp_ingreso` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id_tramite`)) ENGINE = InnoDB;

ALTER TABLE `tramite` ADD INDEX(`rut_usuario_alumno`);

ALTER TABLE `tramite` ADD CONSTRAINT `FK_Tramite_Usuario` FOREIGN KEY (`rut_usuario_alumno`) REFERENCES `usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `tramite` ADD `id_causa` INT NOT NULL AFTER `hora_ingreso`, ADD INDEX (`id_causa`);

ALTER TABLE `tramite` ADD CONSTRAINT `FK_Tramite_Causa` FOREIGN KEY (`id_causa`) REFERENCES `causas`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `archivos` ( `id` INT NOT NULL AUTO_INCREMENT , `id_tramite` INT NOT NULL , `file_ext` VARCHAR(24) NOT NULL , `file_type` VARCHAR(64) NOT NULL , `file_path` VARCHAR(1024) NOT NULL , `orig_name` VARCHAR(64) NOT NULL , `file_name` VARCHAR(64) NOT NULL , `file_size` FLOAT NOT NULL , PRIMARY KEY (`id`), INDEX (`id_tramite`)) ENGINE = InnoDB;

ALTER TABLE `archivos` ADD CONSTRAINT `FK_Archivo_Tramite` FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id_tramite`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `archivos` CHANGE `file_size` `file_size` FLOAT NOT NULL COMMENT 'expresado en kilobytes';

ALTER TABLE `archivos` CHANGE `file_name` `file_name` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nombre almacenado en el servidor';

ALTER TABLE `archivos` CHANGE `orig_name` `orig_name` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'nombre del archivo subido por el usuario';

ALTER TABLE `tramite` ADD `estado_revision` VARCHAR(9) NOT NULL DEFAULT 'PENDIENTE' COMMENT 'PENDIENTE, REVISADO, LEIDO, o CERRADO' AFTER `timestamp_ingreso`;

---------------------
CREATE TABLE `tramite_comentario` ( `id` INT NOT NULL AUTO_INCREMENT , `id_tramite` INT NOT NULL , `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `rut_usuario` INT NOT NULL , `comentario` VARCHAR(4096) NOT NULL , PRIMARY KEY (`id`), INDEX (`id_tramite`), INDEX (`rut_usuario`)) ENGINE = InnoDB;

ALTER TABLE `tramite_comentario` ADD CONSTRAINT `FK_comentario_tramite` FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id_tramite`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `tramite_comentario` ADD CONSTRAINT `FK_comentario_usuario` FOREIGN KEY (`rut_usuario`) REFERENCES `usuarios`(`rut`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `tramite_comentario` ADD `nombre_usuario` VARCHAR(80) NOT NULL AFTER `rut_usuario`;

-------------------------
ALTER TABLE `historial_causas` CHANGE `id_causa` `id_causa` INT NOT NULL COMMENT 'fk a tabla ''causas''';

ALTER TABLE `historial_causas` ADD CONSTRAINT `FK_historialCausa_Causa` FOREIGN KEY (`id_causa`) REFERENCES `causas`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;